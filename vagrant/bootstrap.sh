#!/usr/bin/env bash

if [[ -d /vagrant/.apache ]]
	then
	rm -rf /vagrant/.apache
fi
mkdir -p /vagrant/.xdebug /vagrant/.apache

# download & install wp-cli
mkdir -p /opt/wp-cli
chmod a+w /opt/wp-cli
curl -L https://raw.github.com/wp-cli/builds/gh-pages/phar/wp-cli.phar > /opt/wp-cli/wp-cli.phar
chmod go-w /opt/wp-cli
chmod +x /opt/wp-cli/wp-cli.phar
ln -s /opt/wp-cli/wp-cli.phar /usr/local/bin/wp

# enable mod_rewrite
a2enmod rewrite
# install gd for thumb-generation when creating album-art
#apt-get install php5-gd

# replace default documentroot with wordpress install root
rm -rf /var/www
ln -fs /vagrant_html /var/www

# replace /var/log/apache2 with symlink to vagrant folder
rm -rf /var/log/apache2
ln -fs /vagrant/.apache /var/log/apache2

# pointing enabled default configuration to the one in our repository
ln -fs /vagrant/default.conf /etc/apache2/sites-enabled/000-default
# add our custom php settings
ln -fs /vagrant/php.ini /etc/php5/apache2/conf.d/customsettings.ini

# symlink submodules sub-folders to wordpress paths
#ln -fs /vagrant_lib/wordpressreset/php/public/wp-content/themes/reset /vagrant_html/wp-content/themes/reset

# create an uploads folder with existing content and correct file permissions
mkdir /home/vagrant/uploads
chgrp www-data /home/vagrant/uploads
chmod -R g+w /home/vagrant/uploads
cp -r /vagrant_uploads/* /home/vagrant/uploads/
ln -fs /home/vagrant/uploads /var/www/wp-content/uploads

# re-establish database PNO with user 'pno'@'localhost'
mysql -u root -e "grant all privileges on *.* to 'root'@'%' with grant option;"
mysql -u root -e "flush privileges;"
mysql -u root -e "create database if not exists WordpressReset default character set utf8 default collate utf8_general_ci;"
mysql -u root -e "grant all privileges on WordpressReset.* to 'wordpressreset'@'localhost';"
mysql -u root -e "set password for 'wordpressreset'@'localhost' = password('wordpressreset');"

# wordpress installation with admin credentials: admin/admin (anders.krarup@zuparecommended.dk)
echo "Importing local data from previous datadump. This could take a while..."
mysql -u root WordpressReset < /vagrant_data/mysql-local.sql
echo "Local data imported. Please remember to export the WordpressReset databases into mysql-local.sql if you want the data to persist"
# adjust wordpress to this environment
echo "Updating wordpress data for localhost:9872"
mysql -u root WordpressReset -e "update wp_options set option_value = 'http://localhost:9872' where option_name = 'siteurl' or option_name = 'home';"

service apache2 restart
