<?php
namespace common\wordpress{
	class Output{
		static function printJson($buffer){
			if(stripos($_SERVER['HTTP_USER_AGENT'], 'msie 9')>0){
				header('Content-Type: text/html; Charset: UTF-8');
			}
			else{
				header('Content-Type: application/json; Charset: UTF-8');
			}
			echo json_encode($buffer);
			die();
		}
		
		static function printHtml($buffer){
			header('Content-Type: text/html; Charset: UTF-8');
			echo $buffer;
			die();
		}
	}
}