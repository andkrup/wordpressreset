<?php
namespace net\supervillainhq\wordpressreset{
	class Reset{
		private $options;
		private static $instance;
		public static function instance(){
			if(null==self::$instance){
				self::$instance = new Reset();
			}
			return self::$instance;
		}
		
		private function __construct(){
			$defaults = array();
			$this->options = get_option('wpcmsreset_options', $defaults);
		}
		
		/**
		 * Do installation stuff
		 */
		function onActivation(){
			// clean up database schemas
		}
		function onDeactivation(){
			echo "RESET-DEACTIVATE!";
		}
		
		function onInit(){
			// clean up filters
			remove_all_filters('wp_head');
			add_action('wp_head', 'wp_enqueue_scripts', 2);
			add_action('wp_head', 'locale_stylesheet');
			add_action('wp_head', 'wp_print_styles', 8);
			add_action('wp_head', 'wp_print_head_scripts', 9);
	
			remove_filter('the_content', 'wpautop');
			remove_filter('the_excerpt', 'wpautop');
			remove_filter('comment_text', 'wpautop', 30);
			// sane menu item classes
			remove_all_filters('nav_menu_css_class');
			add_filter('nav_menu_css_class', array($this, 'resetMenuClasses'));
				
			// remove text content editor support for pages
			remove_post_type_support('page', 'editor');
			
			// hide admin bar
			show_admin_bar(false);

			// prepare for invoking the custom hook for adding inline scripts (NOT script-references to an external file) in the <head>
			add_action('wp_head', array(self::instance(), 'printScriptliterals'), 1);
		}
		function printScriptliterals(){
			do_action('svhq_print_scriptliterals', array('default', 'ajax'));
		}
		
		public function resetMenuClasses($list){
			return is_array($list) ? array() : '';
		}
		
		function onAdminMenu(){
			// removes template-select box
			remove_meta_box('pageparentdiv', 'page', 'side');
		}
		
		public function onSetupTheme(){
		}
		
		function onEnqueueScripts(){
			// update jquery version
			if(!is_admin()){
				wp_deregister_script('jquery');
				wp_register_script('jquery', get_template_directory_uri() . '/js/jquery-1.10.2.min.js');
			}
		}
	}
}