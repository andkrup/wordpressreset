<?php
use \net\supervillainhq\wordpressreset\Reset;

/**
 * Autoloader function
 * @param string $classPath absolute class path (including class name)
 * @see http://andkrup.wordpress.com/2013/04/10/autoloader-for-wordpress/
 */
function svhq_load($classPath){
	$base = dirname(__FILE__);
	$paths = array(
		"$base/src"
	);
	foreach($paths as $path){
		$filename = str_replace("\\", '/', "$path/" . __NAMESPACE__ . $classPath . '.php');
		if(is_readable($filename)){
			require $filename;
		}
	}
}
spl_autoload_register(__NAMESPACE__."\\svhq_load");

/**
 * Deflate Wordpress bloat
 */
$wpreset = Reset::instance();
// run this when activating this theme
add_action('after_switch_theme', array($wpreset, 'onActivation'));
add_action('after_setup_theme', array($wpreset, 'onSetupTheme'));
// run this on every REQUEST
add_action('init', array($wpreset, 'onInit'));
add_action('wp_enqueue_scripts', array($wpreset, 'onEnqueueScripts'));

/**
 * UTILITIES
 */

function get_htmlheader($name = null){
	do_action( 'get_htmlheader', $name );
	
	$templates = array();
	$name = (string) $name;
	if ( '' !== $name )
		$templates[] = "htmlheader-{$name}.php";
	
	$templates[] = 'htmlheader.php';
	
	// Backward compat code will be removed in a future release
	if ('' == locate_template($templates, true)){
		load_template( ABSPATH . WPINC . '/theme-compat/htmlheader.php');
	}
}
?>